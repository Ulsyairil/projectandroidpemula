package com.example.ulsyairil.residentevil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import java.util.ArrayList;


public class CardViewResidentEvilAdapter extends RecyclerView.Adapter<CardViewResidentEvilAdapter.CardViewHolder> {

    private ArrayList<ResidentEvil> listResidentEvil;
    private Context context;

    public CardViewResidentEvilAdapter(Context context) {
        this.context = context;
    }

    public ArrayList<ResidentEvil> getListResidentEvil() {
        return listResidentEvil;
    }

    public void setListResidentEvil(ArrayList<ResidentEvil> listResidentEvil) {
        this.listResidentEvil = listResidentEvil;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cardview_residentevil, parent, false);
        CardViewHolder viewHolder = new CardViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CardViewHolder holder, int position) {
        final ResidentEvil r = getListResidentEvil().get(position);
        Glide.with(context).load(r.getPhoto()).apply(new RequestOptions().override(350,550)).into(holder.imgPhoto);
        holder.tvname.setText(r.getName());
        holder.tvremarks.setText(r.getRemarks());
        holder.residentevil = r;
        holder.btnsahre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Intent.ACTION_SEND);
                myIntent.setType("text/plain");
                String shareBody = r.getName();
                String shareSub = r.getDeskripsi();
                myIntent.putExtra(Intent.EXTRA_SUBJECT, shareBody);
                myIntent.putExtra(Intent.EXTRA_TEXT, shareSub);
                context.startActivity(Intent.createChooser(myIntent, "Share using"));
            }
        });
    }

    @Override
    public int getItemCount() {
        return getListResidentEvil().size();
    }

    public class CardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imgPhoto;
        TextView tvname, tvremarks;
        Button btndetail, btnsahre;
        ResidentEvil residentevil;

        public CardViewHolder(View itemView) {
            super(itemView);
            imgPhoto = (ImageView)itemView.findViewById(R.id.img_item_photo);
            tvname = (TextView)itemView.findViewById(R.id.tv_item_name);
            tvremarks = (TextView)itemView.findViewById(R.id.tv_item_remarks);
            btnsahre = (Button)itemView.findViewById(R.id.btn_set_share);
            btndetail = (Button)itemView.findViewById(R.id.btn_set_detail);
            btndetail.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, DetailResidentEvilActivity.class);
            intent.putExtra("key", residentevil);
            context.startActivity(intent);
        }
    }
}
