package com.example.ulsyairil.residentevil;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.*;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class DetailResidentEvilActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_resident_evil);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ResidentEvil diterima = getIntent().getParcelableExtra("key");

        ImageView gamabar = (ImageView)findViewById(R.id.img_item_photo);
        TextView name = (TextView)findViewById(R.id.tv_item_name);
        TextView remarks = (TextView)findViewById(R.id.tv_item_remarks);
        TextView deskripsi = (TextView)findViewById(R.id.content_detail);
        TextView lahir = (TextView)findViewById(R.id.tanggal_rilis);

        Glide.with(this).load(diterima.getPhoto()).apply(new RequestOptions().override(350,550)).into(gamabar);
        name.setText(diterima.getName());
        remarks.setText(diterima.getRemarks());
        deskripsi.setText(diterima.getDeskripsi());
        lahir.setText(diterima.getTanggalRilis());

        Log.i("photo", diterima.getPhoto());
        Log.i("deskripsi", diterima.getDeskripsi());

        // Tombol Kembali Title Bar Berfungsi Kembali
        ActionBar menu = getSupportActionBar();
        menu.setDisplayShowHomeEnabled(true);
        menu.setDisplayHomeAsUpEnabled(true);
    }
}
