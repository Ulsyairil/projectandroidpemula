package com.example.ulsyairil.residentevil;

import android.os.Parcel;
import android.os.Parcelable;

public class ResidentEvil implements Parcelable {
    private String name, remarks, photo, deskripsi, tanggal_rilis;

    protected ResidentEvil (Parcel in) {
        name = in.readString();
        remarks = in.readString();
        photo = in.readString();
        deskripsi = in.readString();
        tanggal_rilis = in.readString();
    }

    public static final Creator<ResidentEvil> CREATOR = new Creator<ResidentEvil>() {
        @Override
        public ResidentEvil createFromParcel(Parcel in) {
            return new ResidentEvil(in);
        }
        @Override
        public ResidentEvil[] newArray(int size) {
            return new ResidentEvil[size];
        }
    };

    public ResidentEvil(){

    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getTanggalRilis() {
        return tanggal_rilis;
    }

    public void setTanggalRilis(String tanggal_rilis) { this.tanggal_rilis = tanggal_rilis; }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.name);
        parcel.writeString(this.remarks);
        parcel.writeString(this.photo);
        parcel.writeString(this.deskripsi);
        parcel.writeString(this.tanggal_rilis);
    }
}